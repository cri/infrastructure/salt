salt-minion-prefix: gpgpu-archlinux

sddm-title: "--- Arch Linux (GPGPU) ---"

path_list:
  - /opt/cuda/NsightCompute-2019.1/

kmod_parameters:
  nvidia: NVreg_RestrictProfilingToAdminUsers=0
