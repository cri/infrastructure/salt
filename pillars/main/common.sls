users:
  epita:
    fullname: Epita
    home: /home/epita
    shell: /bin/bash
    empty_password: True
    groups:
      - audio
      - video

  root:
    password: $6$tpp1raK3wZhH$8O.KfGHijOYyJPNcRMhy6q6WtDfut9oMu/v9mUj3tWQfKfYOkv87bzdYKz/2OsHZza3vsbx8hXTbPmtIBYmK.1
    shell: /bin/bash
    user_files:
      enabled: True
      source: salt://files/default/home/root

sudoers:
  groups:
    wheel:
      - 'ALL=(ALL) ALL'

sddm-footer: "build: {{ "now"|strftime("%y%m%d-%H%m") }}"

sssd_override_shell: /bin/bash
