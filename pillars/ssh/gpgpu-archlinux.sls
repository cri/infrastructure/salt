sshd_config:
  AllowUsers: '*'
  AuthorizedKeysCommand: '/usr/bin/sss_ssh_authorizedkeys'
  AuthorizedKeysCommandUser: 'nobody'
