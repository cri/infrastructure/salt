openssh:
  auth:
    mareo_lab_key:
      - user: root
        present: True
        enc: ssh-rsa
        name: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDRjWonANQ/xE+bAU6e0Wd2s97ONLuHP9EPxdeQTf48NdMOBq/Zyuej8xRd91tHjsF230wMkQemDkSWgEmM9w99yXVt3IOtRizchAQLKEq+3R0eU6gES/gFZ9VL6bNei0jvWAhqNDn7bb/k5FmS+Joy4nsINxmHPBzhJFlcGfENrpUl/lPfWOoldkEjNZ8Wzaxx+OIcvoxsITlOVLu5zD/sRhDS82R6Dr4xPnJxVUxHfmB+ypRTfjA/gBW3JLFxe/GvgpfNpX20OsZPlzyLedW/Km3v3kUFDM5ygAArIAi/LCGohYLF+qkofrj3IM+mxI98ysa8g6SA5jKpC/SA0mZbadUfQJRrFYJp0cJcweMqshqwYG1F4uxm0dv2XTMaoSTn+RixKhIYi9TZK6FWzSZf96tb+n17ZybSv+y+KB1Qa5eJxxaGdFbwO2XAXLtTlhSfPW96AKOSD+d+0N+lJLPOj8HpQiv7+Qq2tUmtNIbelfg7Fzeei9WcsAJvXiHlj5ZOKREsZwe8Z+7gy1XtS57yaq2ogx27vpEYpPqjpX75LSvwxuDBr/5/gEZfDhucLo0LNT9w3mxu6LZCuQB1hNER2IoIADEvizEHyEBguqPYWo7542WJn87nXpy9wwYo7R5Pmv0hVOXCl9NcuEIcZDcvB31cjLLOEf2C546umjMVvw== Marin Hannache (Mareo)"

    melchior_key:
      - user: root
        present: True
        enc: ssh-rsa
        name: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9x9hPa7a9DehZFZeofWNwLKl7vCcqakPhIqon3ULqSv0L2wvEfJMMNMVIE2XWK2zjfWVUgK0JJmpUoLMFk5jSfJd7FadcZdNRbsX8OftXMw9bb7KH0vXc4mz2JQ+PKOzOw93ThIkQaziqWQj33i4euAgg3e/HnwbxtK9JscyJfLo75lsxQj80qMwc7fZz7ljPNcYAOHI7Vw7sT5zZ6/a3FDsgKQSc75gj7onBXMCe8ZnlHhVwSZCmZRQoNJFycs7itV/bwTaBzv4KIl6HXO8KX+M+Hg4T04W9doiPFbM52l0TDFk6Un0uMl00FvBAL68wUQU3eK8wn5BoMlLXS8YRnKGvekGuAYHZSiy2ugv21Yp39vpIyJJ5bhgFTG5CAJHfLnmCJuG/hRLv5xxQHVWCnZuvP5CRQLJeoY3Oe6j1CcVT6H/KFWH1RVU9ax7VYg00KzO3rbVhjGjHtEVbu4scIYykhI2y0HM1aIspg4MfZBa21OAP5reJW7CrcbPfGNxBG/11bGL49bBKf58nl8yl4nMz3CvPdZmKNMdxxBpp+2QBQLBhKJ1kiz1b8i/1LP+P5fXgowXB6OuJweO8PlI/HIa+tcYAPFSoH8ll0SqBoUOQ0qv7vj9DqmaL+g/28VgVoM4UwTrmuJH5Yd3ZaGQKgon2zKI9l0ACNXukybodmw== melchior@yubikey"

    risson_key:
      - user: root
        present: True
        enc: ssh-ed25519
        name: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL0pnnKrvi9lrliSm+pf9HNAzs0GYLKiJk5AtSg4hhDq risson@yubikey"

    rootmout_key:
      - user: root
        present: True
        enc: ssh-ed25519
        name: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICvso1QjsOLrWyVoqkqQjre0TD6LE9djVWTCvkUDjgoe rootmout@cri"

  provide_ecdsa_keys: True
  provide_rsa_keys: True
  ecdsa:
    private_key: |
      -----BEGIN EC PRIVATE KEY-----
      MHcCAQEEIHB6XiqCnU9HMy2+WeZMi4EPxwXFZjosewu6nbvVdjC8oAoGCCqGSM49
      AwEHoUQDQgAEklKLNAiXqTgOfGuVuQlUE50V0eAKHKnKEnsNbz0ydKPd8oPC5taR
      s1vFN3jqwjClQsMPDT975bk8HgroSrDrsw==
      -----END EC PRIVATE KEY-----
    public_key: "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJJSizQIl6k4DnxrlbkJVBOdFdHgChypyhJ7DW89MnSj3fKDwubWkbNbxTd46sIwpULDDw0/e+W5PB4K6Eqw67M= root@blue"

  rsa:
    private_key: |
      -----BEGIN RSA PRIVATE KEY-----
      MIIEowIBAAKCAQEAtF3SdPNcbxb8U49OD75PEKBj8PTOoYrQpW1zkTIc8Oegtv/g
      onZ9Xr6i/KQp5cOkKlKI+A+vcsrTP3lyTo31xVMlH7qh3UmrqUlT7ul9HpRHSfsG
      dzRZnGtshbv85clwFR+V8WuJj7lVXfqzivPWd51fEsFZ6GUMaok0YZgBMeYXsFsd
      QWaN+E1WeNuDi4FFM34E/GuXWkoeK9z6CYkWfIR2U/a71XEjpBH8HJnQbrIKHBUc
      wwnjs9w4elwIoY3e9eO88ghKbICHKjuqREbK4reaNwEsZe+O9tdyEXj7hYOY2Z1T
      ybEmsW3OTHYBY1qAPdc4yC69kILIck3JfLQm+QIDAQABAoIBAD3HpT7J3O9wQB2f
      CG38EbjwN0thjyVmFvGS+/LP53nc3P/RxW/ghjDdu5Yt5Ov+h0y32JKfTMlVD+a+
      AplFptlgEWc81hSmF1Z093ruHx1KTODaSuyp0cfyQ8nLCmTrCqkxmHqJCyVfpg36
      wWTnAJ8YGGikMdeYRRh4xqhnGIGuauxlNY2QFhIdcpXDxDQ+VFWCfaIYXnYLr2aT
      Qb3XHI7OCZD1fq70m5Llr958yM4kglneLGsH9c7DWDBltMDbRNA1bv7QGf32x94J
      SKVskRufvR2SWTuG7WSXbE0GyAbt7f6IdZMdiEB3n6RvXzR4Qk3TUhgMtv9Kcbf1
      f35PSAECgYEA5+k4OKd4efZ4Lq5LigESrz2dZXJg9xNdwI8n46lEb3PgQDWHwxFR
      qdfI5e/ExjA68FH498b7uMkLA6OCJp1Wm5Bv21yjbxqDeYLFmJtDDRbfT2ieozAa
      Dp9JYVH5QYWMbwwvCaJH5wdiMVPsICYbivrrR9ltmaFB+G01Z4OzrzECgYEAxxn5
      c3DPkJs/HVqjYozGb8gAJ2yYlBHkdR/exv2ir7mDRaPTsndEZXVqz7ISFT/0ob7K
      gqrO16LSOM+Tkx3CyQNZ+uXfMOtAURyXYlMyMh3DNX0C2Jt3P0CxFrf7kD3A6imA
      rAGa7qwNzS/WSqkuRsEfwy+52kOJPXjhAq6R0kkCgYEAuVe52AV4Erk5Wc1wj63F
      2gn7Ne2Qs0ZmAhQnRBqumAha69YcV3kK9BuB8sqKT/6RzCknOhdsbEU4qULPLL9I
      TXHFA+Lot03QT5eFBi7oCpkCBbvONQund5I0GjRiacwq7UEmgqOQpYJQJq40o4jx
      METTLwwFPOgVCk91g/JM87ECgYAOKov0Kceq+1DPTt6nKWsJEwrnwzNL/cXqi3z3
      XZ6QsDtl3hU28oN8UJFr+7iLqIRIaeHQ0ERtVVltz18prYlpcgKZ9ncd7O8jlkwz
      I8ZDiJiWGrH0JYOjjX/MJQUSK0KpppBQLMEQUE+fFNNDdqwjPqR6589qdzmxlQM1
      hKQ9eQKBgFq8gJVUil2DJLFmLpfoJ1FH3R/3c3cApjcka1LKSZh23vTGvC4hLmVP
      d6R9QgFpTnvT9SqhizvVqrmskttA/Oaj5NZHeCjZyK3FTCSMNTSAgWECM34pmJBC
      1Ygpy8i3y1nYEzxU10CTxjY4giYc/0OwIAdABuU5IpnvlLbmYNo8
      -----END RSA PRIVATE KEY-----
    public_key: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC0XdJ081xvFvxTj04Pvk8QoGPw9M6hitClbXORMhzw56C2/+Cidn1evqL8pCnlw6QqUoj4D69yytM/eXJOjfXFUyUfuqHdSaupSVPu6X0elEdJ+wZ3NFmca2yFu/zlyXAVH5Xxa4mPuVVd+rOK89Z3nV8SwVnoZQxqiTRhmAEx5hewWx1BZo34TVZ424OLgUUzfgT8a5daSh4r3PoJiRZ8hHZT9rvVcSOkEfwcmdBusgocFRzDCeOz3Dh6XAihjd7147zyCEpsgIcqO6pERsrit5o3ASxl747213IRePuFg5jZnVPJsSaxbc5MdgFjWoA91zjILr2QgshyTcl8tCb5 root@blue"

sshd_config:
  PermitRootLogin: 'prohibit-password'
  PermitEmptyPasswords: 'no'
  AllowUsers: 'root'
  UsePam: 'yes'
  AuthorizedKeysFile: '.ssh/authorized_keys'
  Subsystem: 'sftp    /usr/lib/ssh/sftp-server'
  ChallengeResponseAuthentication: 'no'
  X11Forwarding: 'yes'

ssh_config:
  Hosts:
    exam.pie.cri.epita.fr:
      GSSAPIAuthentication: yes
    git.cri.epita.fr:
      GSSAPIAuthentication: yes
    ssh.cri.epita.fr:
      GSSAPIAuthentication: yes
      GSSAPIDelegateCredentials: yes
