base:
  '*':
    - main.common
    - ssh.common
    - pkgs.common
    - pkgs.fuse
    - pkgs.yubikey

  'pie-archlinux-*':
    - main.pie-archlinux
    - pkgs.desktop
    - pkgs.dev
    - pkgs.dev-ruby
    - pkgs.machine-state-led
    - pkgs.misc
    - pkgs.pie-archlinux
    - pkgs.xorg
    - pkgs.vbox
    - pkgs.dev-sql
    - pkgs.dev-tc
    - pkgs.dev-spider
    - pkgs.dev-java
    - pkgs.dev-webservices
    - pkgs.sante

  'spe-archlinux-*':
    - main.spe-archlinux
    - pkgs.cd68k
    - pkgs.desktop
    - pkgs.dev
    - pkgs.dev-asm
    - pkgs.dev-ruby
    - pkgs.dev-rust
    - pkgs.dev-sdl
    - pkgs.misc
    - pkgs.pie-archlinux
    - pkgs.xorg

  'ji-archlinux-*':
    - main.ji-archlinux
    - pkgs.desktop
    - pkgs.dev
    - pkgs.doc
    - pkgs.misc
    - pkgs.sup-archlinux
    - pkgs.ji-archlinux

  'hadoop-archlinux-*':
    - main.hadoop-archlinux
    - dev.spark.spark-1-6-3
    - pkgs.cd68k
    - pkgs.desktop
    - pkgs.dev
    - pkgs.dev-java
    - pkgs.dev-scala
    - pkgs.misc
    - pkgs.pie-archlinux
    - pkgs.xorg
    - ssh.hadoop

  'sup-archlinux-*':
    - main.sup-archlinux
    - pkgs.desktop
    - pkgs.dev
    - pkgs.doc
    - pkgs.misc
    - pkgs.ocaml
    - pkgs.sup-archlinux
    - pkgs.xorg

  'browser-archlinux-*':
    - main.browser-archlinux
    - pkgs.browser-archlinux
    - pkgs.xorg

  'dump-*':
    - main.dump
    - pkgs.dump

  'exec-archlinux-*':
    - main.exec-archlinux

  'fioi-archlinux-*':
    - main.fioi
    - pkgs.pie-archlinux
    - pkgs.desktop
    - pkgs.dev
    - pkgs.dev-sdl
    - pkgs.fioi
    - pkgs.misc
    - pkgs.ocaml
    - pkgs.xorg

  'cri-archlinux-*':
    - main.cri-archlinux
    - pkgs.cd68k
    - pkgs.cri-archlinux
    - pkgs.desktop
    - pkgs.dev
    - pkgs.dev-iso
    - pkgs.dev-ruby
    - pkgs.dev-sdl
    - pkgs.dev-torrent
    - pkgs.xorg

  'gpgpu-archlinux-*':
    - main.gpgpu-archlinux
    - ssh.gpgpu-archlinux
    - pkgs.desktop
    - pkgs.dev
    - pkgs.gpgpu
    - pkgs.misc
    - pkgs.pie-archlinux
    - pkgs.xorg
    - pkgs.opengl

  'ricou-archlinux-*':
    - main.ricou-archlinux
    - pkgs.desktop
    - pkgs.dev
    - pkgs.dev-ruby
    - pkgs.misc
    - pkgs.pie-archlinux
    - pkgs.python-bigdata
    - pkgs.python-ml
    - pkgs.xorg

  'test-archlinux-*':
    - main.pie-archlinux
    - pkgs.desktop
    - pkgs.dev
    - pkgs.dev-ruby
    - pkgs.machine-state-led
    - pkgs.misc
    - pkgs.pie-archlinux
    - pkgs.xorg
    - pkgs.vbox
    - pkgs.dev-tc
    - pkgs.dev-spider
    - pkgs.dev-java
    - pkgs.dev-webservices
    - pkgs.sante

  'covid-archlinux-*':
    - pkgs.xorg

  'recrutements-archlinux-*':
    - main.recrutements-archlinux
    - ssh.recrutements-archlinux
    - pkgs.desktop
    - pkgs.dev
    - pkgs.dev-ruby
    - pkgs.pie-archlinux
