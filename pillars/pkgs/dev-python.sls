pkgs:
  - ipython
  - jupyter
  - mathjax
  - pycharm-community-edition
  - python
  - python2
  - python-pip
