pkgs:
  # OpenCL
  - clinfo
  - ocl-icd
  - opencl-headers
  - opencl-mesa

  # CUDA and OpenCL (despite the names)
  - nvidia-dkms
  - opencl-nvidia
  - cuda
  - cuda-tools
  - jre8-openjdk
  - ncurses5-compat-libs
  - opencv

  # Misc
  - hashcat
  - hashcat-utils
  - glfw-x11
  - freeimage
  - boost
