pkgs:
  - intellij-idea-ultimate-edition
  - jdk
  - jdk11-openjdk
  - maven
  - npm
  - protobuf
