pkgs:
  # audio
  - pulseaudio
  - pavucontrol
  - paprefs
  - pulseaudio-bluetooth

  # bluetooth
  - bluez
  - bluez-utils
  - bluez-tools
  - blueberry

  # browsers
  - firefox
  - firefox-i18n-fr
  - chromium

  # communication
  - claws-mail
  - irssi
  - msmtp
  - mutt
  - thunderbird
  - weechat

  # editors
  - emacs

  # images
  - feh
  - gimp
  - imagemagick
  - scrot

  # misc
  - bc
  - dialog
  - i3lock-cri
  - keepassxc
  - mlocate
  - openbsd-netcat
  - redshift
  - rlwrap
  - rxvt-unicode
  - xorg-xeyes
  - xorg-xkill
  - zenity

  # pdf reader
  - evince
  - zathura-pdf-poppler

  # video tools
  - vlc

  # back to home
  - teams
  - discord
  - slack-desktop
