pkgs:
  - vcsn-git
  - jupyter
  - jupyter-notebook
  - python-regex
  - python-colorama
  - yaml-cpp
