pkgs:
  - bochs
  - bochs-gdb
  - grub
  - libisoburn
  - mtools
  - qemu
  - qemu-arch-extra
