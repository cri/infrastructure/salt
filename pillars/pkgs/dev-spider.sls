pkgs:
  - autoconf-archive
  - criterion
  - gtest
  - libev
  - mkcert
  - siege
  - wrk
  - zlib
