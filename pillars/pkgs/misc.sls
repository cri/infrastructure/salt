pkgs:
  - csfml
  - freeglut
  - perf
  - python-tensorflow
  - sfml
  - tensorflow
  - texlive-fontsextra
