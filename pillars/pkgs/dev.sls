pkgs:
  # build systems
  - autoconf
  - automake
  - autoconf-archive
  - cmake
  - make

  # compilers
  - clang
  - gcc
  - llvm

  # misc
  - capstone
  - check
  - criterion
  - ctags
  - doxygen
  - fakeroot
  - fff-git
  - flex
  - gdb
  - rr-bin
  - lcov
  - ltrace
  - shellcheck
  - strace
  - tk
  - valgrind
  - dash
  - checkbashisms

  # lcov dependencies
  - perl-json
  - perl-perlio-gzip

  # vcs
  - subversion
  - git
  - tig

pip:
  - pre-commit
