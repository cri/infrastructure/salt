dev-tc-add-custom-bison-to-path:
  file.line:
    - name: /etc/profile
    - content: PATH="/opt/bison-epita/bin:$PATH"
    - mode: insert
    - location: start
