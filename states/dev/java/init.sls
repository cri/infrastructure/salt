dev-java-add-intelij-jdk-env:
  file.line:
    - name: /etc/profile
    - content: 'export IDEA_JDK=/usr/lib/jvm/java-11-openjdk/'
    - mode: insert
    - location: start
