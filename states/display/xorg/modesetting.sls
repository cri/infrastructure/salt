disaply-xorg-configure-modesetting:
  file.managed:
    - name: /etc/X11/xorg.conf.d/modesetting.conf
    - source: salt://display/xorg/files/modesetting.conf
