display-wm-i3-i3lock-cri-install:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - name: i3lock-cri

display-wm-i3-i3lock-cri-configure:
  file.managed:
    - name: /usr/share/backgrounds/lock.png
    - source: {{ salt['pillar.get']('i3-background', 'salt://display/wm/i3/files/background.jpg') }}
    - makedirs: True
    - require:
        - pkg: display-wm-i3-i3lock-cri-install
