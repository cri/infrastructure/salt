display-wm-xfce-tools-install:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - thunar-archive-plugin
      - xarchiver
      - zip
      - unzip
      - unrar
