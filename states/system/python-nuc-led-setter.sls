system-nuc-led-pkgs:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - intel_nuc_led-dkms-git
      - python-nuc-led-setter

system-nuc-led-enable:
  service.running:
    - name: nuc_led_setter
    - enable: True
    - require:
      - pkg: system-nuc-led-pkgs
