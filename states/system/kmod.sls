system-kmod:
  file.managed:
    - name: /etc/modprobe.d/cri-kmod.conf
    - source: salt://system/files/cri-kmod.conf.jinja2
    - template: jinja
    - mode: 644
    - context:
      kmod_parameters: {{ salt['pillar.get']('kmod_parameters', {}) }}
