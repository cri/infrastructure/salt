system-path:
  file.managed:
    - name: /etc/profile.d/cri-path.sh
    - source: salt://system/files/cri-path.sh.jinja2
    - template: jinja
    - mode: 755
    - context:
      path_list: {{ salt['pillar.get']('path_list', []) }}
