system-yubikey-install-pkgs:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - ccid
      - opensc
      - pcsc-tools

system-yubikey-enable-pcscd:
  service.running:
    - name: pcscd
    - enable: True
    - require:
      - pkg: system-yubikey-install-pkgs

system-yubikey-udev-rule:
  file.managed:
    - name: /etc/udev/rules.d/70-yubikey.rules
    - source: salt://system/files/70-yubikey.rules
    - mode: 644
