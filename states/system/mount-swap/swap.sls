system-mount-swap:
  file.managed:
    - name: /etc/systemd/system/mount-swap.service
    - source: salt://system/mount-swap/files/mount-swap.service

mount-swap-service:
  service.enabled:
    - name: mount-swap
