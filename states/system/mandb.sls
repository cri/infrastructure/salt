system-mandb-install-mandb:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - name: man-db

system-mandb-update:
  cmd.run:
    - name: mandb
    - require:
      - pkg: system-mandb-install-mandb
