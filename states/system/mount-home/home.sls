system-mount-home:
  file.managed:
    - name: /etc/systemd/system/mount-home.service
    - source: salt://system/mount-home/files/mount-home.service

mount-home-service:
  service.enabled:
    - name: mount-home
