system-machine-state-pkgs:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - python-machine-state

system-machine-state-enable:
  service.running:
    - name: org.cri.MachineState
    - enable: True
    - require:
      - pkg: system-machine-state-pkgs

