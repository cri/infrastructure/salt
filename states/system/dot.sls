system-dot-install-graphviz:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - name: graphviz

system-dot-configure:
  cmd.run:
    - name: dot -c
    - require:
      - pkg: system-dot-install-graphviz
