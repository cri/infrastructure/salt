system-ecryptfs-install:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - ecryptfs-utils

system-ecryptfs-configure:
  file.managed:
    - name: /etc/modules-load.d/ecryptfs.conf
    - contents:
       - ecryptfs
    - require:
      - pkg: system-ecryptfs-install
