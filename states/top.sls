base:
  '*':
    - meta.commons

  'G@virtual_subtype:chroot or *arch_creator':
    - match: compound
    - meta.arch_creator

  'pie-archlinux-*':
    - meta.pie
    - meta.xorg
    - network.nswrappers
    - dev.tcpdump
    - dev.tc
    - dev.java
    - system.python-machine-state
    - system.python-nuc-led-setter

  'spe-archlinux-*':
    - meta.pie
    - meta.xorg
    - system.docker
    - display.wm.xfce
    - dev.tcpdump

  'ji-archlinux-*':
    - match: glob
    - meta.pie
    - meta.xorg
    - display.wm.xfce

  'sup-archlinux-*':
    - meta.pie
    - meta.xorg
    - misc.git-scripts
    - display.wm.xfce
    - dev.tuareg

  'browser-archlinux-*':
    - meta.xorg-nodm
    - auth.autologin

  'dump-*':
    - auth.exec

  'exec-archlinux-*':
    - auth.exec
    - system.fstab.nomounts

  'fioi-archlinux-*':
    - meta.pie
    - meta.xorg
    - display.wm.xfce
    - dev.tuareg

  'cri-archlinux-*':
    - meta.pie
    - meta.xorg
    - system.sudo_all
    - dev.tcpdump

  'gpgpu-archlinux-*':
    - meta.pie
    - meta.xorg
    - display.wm.xfce

  'ricou-archlinux-*':
    - meta.pie
    - meta.xorg
    - display.wm.xfce

  'test-archlinux-*':
    - meta.pie
    - meta.xorg
    - network.nswrappers
    - dev.tcpdump
    - dev.tc
    - dev.java
    - system.python-machine-state
    - system.python-nuc-led-setter

  'covid-archlinux-*':
    - meta.pie
    - meta.xorg-nodm
    - misc.foldingathome
