network-nswrappers-pkgs:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - ethtool
      - libcap

network-nswrappers-files:
  file.recurse:
    - name: /usr/local/bin/
    - source: salt://network/nswrappers/files/
    - file_mode: 0755
    - require:
      - pkg: network-nswrappers-pkgs
