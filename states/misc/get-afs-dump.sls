misc-get-afs-dump-pkgs:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - curl
      - jq

misc-get-afs-dump-script:
  file.managed:
    - name: /usr/local/bin/afs-restore
    - source: salt://misc/files/afs-restore.sh
    - mode: 755
    - require:
      - pkg: misc-get-afs-dump-pkgs
