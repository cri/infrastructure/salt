misc-foldingathome-install-pkgs:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - foldingathome

misc-foldingathome-enable-service:
  service.running:
    - name: foldingathome
    - enable: True
    - require:
      - pkg: misc-foldingathome-install-pkgs

misc-foldingathome-config-file:
  file.managed:
    - name: /opt/fah/config.xml
    - source: salt://misc/files/foldingathome-config.xml
    - mode: 644
