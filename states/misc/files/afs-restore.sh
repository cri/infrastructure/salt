#!/usr/bin/env bash

set -e

usage()
{
    echo -e "$(basename "$0") [-d|--dest <directory>] [-h|--help]"
    echo
    echo -e "               [-d|--dest <directory>] extract the afs volume \
to <directory> and skip the dotfiles installation"
    echo -e "               [-h|--help] display this help message"
    exit "${1:-1}"
}

# Vars
AFS_DUMP_NAME="afs_dump"
AFS_DIR_NAME="afs"
RESTORE_PATH="/home/epita"
U_DIR="/u"

while [ $# -gt 0 ]
do
    case "${1}" in
    -d|--dest)
        U_DIR=""
        skip_install="true"
        if [ -z "${2}" ]; then
            echo "You must specify a path after this option."
            exit 1
        fi
        RESTORE_PATH="${2}"
        shift 2
        break
        ;;
    -h|--help)
        usage 0
        ;;
    *)  # unknown option
        echo "Unknow option, exiting..."
        usage
        ;;
    esac
done

# Ensure that the user is aware of the fact, that this operation can overwrite
# files, default to `No`.
read -r -p "This operation will download your AFS to ${RESTORE_PATH}/\
${AFS_DIR_NAME}, do you want to procede ? (y/N): " user_ack

case $user_ack in
    [Yy])
        ;;
    *)
        exit 0
        ;;
esac

# Get user credentials
echo "Please insert your CRI credentials:"
while [ -z "$cri_login" ]; do
    read -r -p "login: " cri_login;
done
while [ -z "$cri_password" ]; do
    read -r -sp "password: " cri_password;
done

echo

# Connect to the API and retrieve the S3 link
tmp_file_link="$(mktemp -p /tmp afs_link-XXXXX)"

curl_link_rc="$(curl -s -u "$cri_login":"$cri_password" -o "${tmp_file_link}" -w "%{http_code}" https://cri.epita.fr/api/afs/ 2> /dev/null)"

if [ "${curl_link_rc}" != "200" ]; then
    echo "Unable to get the link to your afs, aborting."
    jq .detail < "${tmp_file_link}"
    exit 2
else
    S3_AFS_LINK="$(jq -r .url < "${tmp_file_link}")"
fi

# Create a tmp dir for the AFS archive
tmp_dir_dl="$(mktemp -d -p "$HOME" afs_archive-XXXXX)"

# Get the AFS archive with the S3 link
echo "Downloading your AFS"
curl_dl_rc="$(curl -s -o "${tmp_dir_dl}/$AFS_DUMP_NAME.xz" -w "%{http_code}" "$S3_AFS_LINK" 2> /dev/null)"

if [ "${curl_dl_rc}" != "200" ]; then
    echo "Unable to get your afs archive, aborting."
    rm -rf "${tmp_dir_dl}/$AFS_DUMP_NAME.xz"
    exit 3
fi

# Decompress AFS archive
echo "Decompressing your AFS"
xz -d "${tmp_dir_dl}/$AFS_DUMP_NAME.xz"

# Extract the files from the AFS volume to ${RESTORE_PATH}/${AFS_DIR_NAME}
# Don't forget the 'u' folder inside the afs volume

mkdir -p "${RESTORE_PATH}"
mv "${RESTORE_PATH}/${AFS_DIR_NAME}" "${RESTORE_PATH}/${AFS_DIR_NAME}.$(date +%s)" 2> /dev/null || true

# Create a tmp dir for the AFS volume located on the disk
tmp_dir_restore="$(mktemp -d -p "${RESTORE_PATH}" afs_volume-XXXXX)"

restorevol -file "${tmp_dir_dl}/${AFS_DUMP_NAME}" -dir "${tmp_dir_restore}"

# We keep the `u` dir when U_DIR=""
mv "${tmp_dir_restore}/"*"${U_DIR}" "${RESTORE_PATH}/${AFS_DIR_NAME}"

# Execute the `install.sh` script
if [ -z "$skip_install" ]; then
    AFS_DIR="${RESTORE_PATH}/${AFS_DIR_NAME}" ${RESTORE_PATH}/${AFS_DIR_NAME}/.confs/install.sh
    chmod -R og-rw "${RESTORE_PATH}/${AFS_DIR_NAME}/.confs/ssh"
fi

rm -rf "${tmp_file_link}" "${tmp_dir_dl}" "${tmp_dir_restore}"

echo "AFS restoration completed."
