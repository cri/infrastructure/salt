auth-sso-spnego-install-firefox:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - firefox

auth-sso-spnego-configure-firefox-local-settings:
  file.managed:
    - name: /usr/lib/firefox/defaults/pref/local-settings.js
    - source: salt://auth/sso/files/local-settings.js
    - require:
      - pkg: auth-sso-spnego-install-firefox

auth-sso-spnego-configure-firefox-mozilla-cfg:
  file.managed:
    - name: /usr/lib/firefox/mozilla.cfg
    - source: salt://auth/sso/files/mozilla.cfg
    - require:
      - pkg: auth-sso-spnego-install-firefox

auth-sso-spnego-install-chromium:
  pkg.installed:
    - sysupgrade: False
    - refresh: False
    - pkgs:
      - chromium

auth-sso-spnego-configure-chromium:
  file.managed:
    - name: /etc/chromium/policies/recommended/spnego.json
    - source: salt://auth/sso/files/spnego.json
    - makedirs: True
    - require:
      - pkg: auth-sso-spnego-install-chromium
