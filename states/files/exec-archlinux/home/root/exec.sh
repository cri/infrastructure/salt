#!/bin/sh

EXEC_URL=$(cat /proc/cmdline | sed "s/.*exec_url=\([^ ]*\).*/\1/")
wget "${EXEC_URL}" -O /tmp/script.sh
chmod +x /tmp/script.sh
/tmp/script.sh
echo "Shutdown in 10s"
read -t 10 -p "Hit ENTER to drop into a shell" || poweroff
exec /bin/sh
